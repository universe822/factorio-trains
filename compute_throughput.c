/*
This program computes train throughput and export values to "output.csv".
*/

#include <stdio.h>
#include <math.h>  // fminf, log(), pow()

float air_resistance=0.0075; // loco first

/*
 * Arithmetic-geometric progression.
 */
int compute_acceleration(float max_speed, float r, float aero_term, float *speed, float *position, float *tick){
	
	*tick = log( (max_speed - r) / (*speed - r) ) / log(aero_term);
	*position = (*speed - r) * (1 - pow(aero_term, *tick)) / (1 - aero_term) + *tick * r;
	*speed = max_speed;

	return 0;
}

/*
 * Arithmetic progression.
 */
int compute_braking(float braking_constant, float *speed, float *position, float *tick){

	*tick = *speed / braking_constant;
	*position = (*tick + 1) * (*speed / 2);
	*tick += 10; // Simplistic offset for non-linear behavior of braking train

	return 0;
}

/*
 * Computes the throughput of a specific train over a travel cycle.
 * Outputs a value in blue belts (45 item/s).
 */
float compute_throughput(
	int loco,         // A 1:1:1 train has two locomotives
	int usfl,         // A 1:1:1 train has one useful locomotive (while accelerating)
	int wago,         // A 1:1:1 train has one wagon
	int fuel,         // Train fuel (0:coal, 1:solid fuel, 2:rocket fuel, 3:nuclear fuel)
	int wslot,        // Wagon slots (usually 40, but only 1 for fluid wagons)
	int wss,  	    // Wagon Stack Size (10, 50, 100, 200, but 25k for fluid wagons)
	int insi,         // The number of stack inserters or pumps per wagon (loading station)
	int inso,         // The number of stack inserters or pumps per wagon (unloading station)
	int inst,         // Inserter Type (0:stack, 1:pumps)
	int insr,         // Inserter Research (0-7)
	int brar,         // Braking Research (0-7)
	int chunk) {      // Distance from loading to unloading station (32 tiles)
	
	float speed;      // main speed variable of the train (tile/tick)
	float vmax;       // maximum speed reached after acceleration (tile/tick)
	float max_speed;  // configurable speed limit
	
	float position;   // main position variable of the train (tile)
	float accel_x;    // acceleration distance (tile)
	float brake_x;    // braking distance (tile)
						
	float tick;       // main time variable of the train (tick)
	float accel_t;    // acceleration time (tick)
	float coast_t;    // coasting time (tick)
	float brake_t;    // braking time (tick)
	float load_t;     // Time in tick for a train to be fully loaded
	float unload_t;   // Time in tick for a train to be fully unloaded

	float items;      // All the items moved by the train
	float cycle_t;    // The cycle time in ticks
	float throughput; // Train throughput over a cycle

	// Compute train values
	int weight = loco * 2000 + wago * 1000;
	float friction = (loco + wago) * 0.5;
	float acceleration_multiplier[4] = {1, 1.2 , 1.8 , 2.5};
	float top_speed_multiplier[4] =    {1, 1.05, 1.15, 1.15};
	float speed_cap = top_speed_multiplier[fuel] * 1.2;

	float friction_term = friction / weight;
	float accel_term = 10 * usfl * acceleration_multiplier[fuel] / weight;
	float aero_term = 1 - (air_resistance * 1000 / weight);
	float r = aero_term * (accel_term - friction_term) / (1 - aero_term);

	int braking_force = loco * 10 + wago * 3;
	float braking_multiplier[8] = {1, 1.1, 1.25, 1.4, 1.55, 1.7, 1.85, 2};
	float braking_constant = (braking_force * braking_multiplier[brar] + friction) / weight;

	// Load and unload time in tick
	float stack_inserter_throughput[8] = {4.615, 6.9225, 9.23, 11.5375, 13.845, 18.46, 23.075, 27.69};
		
	if (inst == 0){ 
		// Stack inserters are used
		load_t   = (60 * wss * wslot) / (stack_inserter_throughput[insr] * insi);
		unload_t = (60 * wss * wslot) / (stack_inserter_throughput[insr] * inso);
	}
	else { 
		 // Pumps
		 load_t   = ( (60 * wss * wslot) / (12000 * insi) ) + 64; // Pump animations last 64 ticks.
		 unload_t = ( (60 * wss * wslot) / (12000 * inso) ) + 64;
		 }

	// Acceleration time in tick
	speed = 0;
	position = 0;
	tick = 0;

	/* 
	 * In theory, the train will reach a max_speed of "r" for infinite ticks. In practice, 0.99 * r gives good results.
	 * We must not forget the train speed cap, they sadly can't go to their natural 700km/h.
	 */
	max_speed = fminf(r * 0.99, speed_cap);

	// Train sanity check : can you even accelerate?
	if (chunk == 0) return 0;
	if (accel_term > friction_term){
	
		compute_acceleration(max_speed, r, aero_term, &speed, &position, &tick);
		vmax = speed;
		accel_x = position;
		accel_t = tick;
	}
	else{
		printf("%d:%d train can't accelerate.\n", usfl, wago);
		return 0;
	}

	// Braking time in tick
	position = 0;
	tick = 0;
	
	compute_braking(braking_constant, &speed, &position, &tick);
	brake_x = position;
	brake_t = tick;
	
	// Coast time in tick (simple case) : the train has enough rail length (chunks) to reach terminal velocity and brake
	if (accel_x + brake_x <= chunk * 32){ 
		coast_t = ((chunk * 32) - accel_x - brake_x) / vmax;
	}
	
	// Coast time in tick (less simple) : the train has to stop accelerating and start braking to avoid station overshoot. 
	// We are decreasing the max speed of the train (called "limit") until it stops at about the right position. 
	// Old acceleration and braking times are updated.
	else {
		coast_t = 0;
		
		while(accel_x + brake_x > chunk * 32){

			speed = 0;
			position = 0;
			tick = 0;

			// We lower max speed until the train doesn't overshoot its destination anymore 
			max_speed -= 1E-3;
			compute_acceleration(max_speed, r, aero_term, &speed, &position, &tick);
			
			// New acceleration time in tick
			accel_x = position;
			accel_t = tick;

			position = 0;
			tick = 0;

			// New braking time in tick
			compute_braking(braking_constant, &speed, &position, &tick);
			brake_x = position;
			brake_t = tick;
		}
	}

	// train throughput
	items = wago * wss * wslot;
	cycle_t = load_t + unload_t + (accel_t + coast_t + brake_t) * 2;
	throughput = 60 * items / cycle_t;

	//compute_fuel_consumption(usfl, fuel, accel_t, coast_t, cycle_t);
	
	return throughput / 45.0;
}

int compare_train_configs(void){

	int chunk, chunk1, chunk2, train, i;

	// Graph limits
	printf("Chunk distance between stations :\n");
	printf("Minimum  (10) ");  scanf("%d", &chunk1); 
	printf("Maximum  (300) "); scanf("%d", &chunk2); 
	printf("How many trains to compare? (1)     "); scanf("%d", &train);
	int loco[train], usfl[train], usfl2[train], wago[train], fuel[train], wslot[train], wss[train], insi[train], inso[train], inst[train], insr[train], brar[train];

	// Inputing information on each train
	for (i = 0; i < train; i++) {
		printf("\nSpecific values of train %d\n", i+1);
		printf("Total locomotives               "); scanf("%d", &loco[i]);
		printf("Useful locomotives              "); scanf("%d", &usfl[i]);
		printf("Wagons                          "); scanf("%d", &wago[i]);
		printf("Fuel (3:nuclear)                "); scanf("%d", &fuel[i]);
		printf("Wagon slots (1-40)              "); scanf("%d", &wslot[i]);
		printf("Stack size  (10-25k)            "); scanf("%d", &wss[i]);
		printf("Inserters/pumps per wagon (In)  "); scanf("%d", &insi[i]);
		printf("Inserters/pumps per wagon (Out) "); scanf("%d", &inso[i]);
		printf("Inserter type (0:stack, 1:pump) "); scanf("%d", &inst[i]);
		printf("Inserter research (0-7)         "); scanf("%d", &insr[i]);
		printf("Braking research  (0-7)         "); scanf("%d", &brar[i]);
	}

    // Open a new output.csv
    FILE *fptr;
    fptr = fopen("output.csv", "w");
	fprintf(fptr, "Chunk,");
	
	/*
	 * Writes each train configuration in text, on the first line of output.csv
	 * This is mandatory to display the gnuplot curve titles correctly, 
	 * using the "columnhead" option in compare_train_configs.gnu
	 */
	for (i = 0; i < train; i++) {
	
		if (loco[i] - usfl[i] > 0) usfl2[i] = usfl[i]; else usfl2[i] = 0; // Correctly display m:n:m trains 
		fprintf(fptr, "%d:%d:%d f%d wslot%d wss%d i%d o%d t%d ir%d br%d,", usfl[i], wago[i], usfl2[i], fuel[i], wslot[i], wss[i], insi[i], inso[i], inst[i], insr[i], brar[i]); 
	}

	// Writes chunk and throughput values in output.csv
	for (chunk = chunk1; chunk <= chunk2; chunk++) {
	
		fprintf(fptr, "\n%d,", chunk);
		for (i = 0; i < train; i++) {
			fprintf(fptr, "%.3f,", compute_throughput(loco[i], usfl[i], wago[i], fuel[i], wslot[i], wss[i], insi[i], inso[i], inst[i], insr[i], brar[i], chunk));
		}
	}
	fclose(fptr);

	return 1;
}

int main(void){
	
	compare_train_configs();

   return 0;
}
