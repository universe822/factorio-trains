# C factorio 1.1 train throughput calculator on Linux
This project computes train throughput of a single factorio train (eg. 1:4, 4:16:4, etc) over a train cycle. I use Gnuplot to quickly and automatically draw a graph from the generated throughput values in blue belts.
![The throughput in blue belts of an endgame 1:1 train](example.png)
This is the resulting throughput of a 1:1 train with nuclear fuel, cargo wagons, 50 stack size, 4 stack inserters per wagon (loading and unloading) and max research. 
You can also compare the throughput of several and very different trains on the same graph.

## Project assumptions

- One train on the network, following a simple cycle.
- One input station and one output station.
- Only one type of inserter in both stations.
- Chunk-aligned rails only, to simplify distance between stations.
- Train has front facing locomotive (can be changed with air_resistance variable).
- Locomotives are symmetrical, always 1:3:1, 4:15:4, etc. Never 2:2:3, 1:5:5, etc.
- Locomotives are fueled during loading or unloading.
- Fuel types : coal, solid fuel, rocket fuel, nuclear fuel.
- Belts might as well not exist, I only care about train throughput.
- To load wagons we use either stack inserters or pumps, and I assume a fluid wagon has one slot and a stack size of 25000.
- I assume all trains can accelerate (acceleration term > friction term), meaning there isn't too many wagons per locomotive.

The following assumptions ensures fast loading/unloading times :
- Each station's loading and unloading is balanced.
- Input station chests have enough items to fill each wagon. 
- Output station chests can empty each wagon.
- For fluid trains, the classic 'tanks > pumps > wagon' setup is used.
- Therefore, all inserters/pumps are working at 100% while loading or unloading. 

## Train cycle

The train follows a repeating cycle of different phases :
- Load the wagons at an input station.
- Accelerate.
- Coast at terminal velocity if applicable. "coast" is probably a bad name, I call coasting the behavior where the train is still accelerating but reached terminal velocity.
- Brake.
- Unload the wagons at an output station.
- Accelerate again.
- Coast at terminal velocity again if applicable.
- Brake.

## Making your own graphs

0. Install the gcc and gnuplot packages.
1. On the terminal, compile and execute :
```
$ gcc compute_throughput.c -o compute_throughput -lm
$ ./compute_throughput
```
2. Enter the required input information, press Enter until the program ends.
3. Data is saved on output.csv file.
4. Execute "graph.gnu" to generate a gnuplot graph from that file (must have execute permission). You can also use any spreadsheet software to plot things manually.

## Model precision
![In-game values vs. program output](validation.png)
Here I compare the output of my program for 3 trains (1:1, 1:4, 4:16:4), to a few in-game measurements I did. 
Those measurement's precision is pretty bad because factorio's production tab rounds values too much.

Still, I'm happy with the model.

## General information

Train speed is in tile/tick.\
A tick is 1/60th of a second, and a tile is equivalent to one meter. A chunk is 32 tiles.\
x * 216 (tile/tick) = y (km/h).\
1.38 (tile/tick) ~ 298.1 (km/h).\

Relevant locomotive data in trains.lua :
* weight = 2000
* friction_force = 0.50
* braking_force = 10
* air_resistance = 0.0075
* max_speed = 1.2

Relevant cargo/fluid wagon data in trains.lua :
* weight : 1000
* friction_force = 0.50
* braking_force = 3
* air_resistance = 0.01

Source : /factorio/data/base/prototypes/entity/train.lua

## Acceleration function
You can compute the acceleration time and acceleration distance with a loop like this :

	int acceleration_loop(int loco, int usfl, int wagon, int fuel, float *speed, float *position, int *tick){
		
		float air_resistance=0.0075; // loco first
		float epsilon = 2.4E-7; // Small empirical value
		int weight = loco * 2000 + wagon * 1000;
		float friction = (loco + wagon) * 0.5;
		float acceleration_multiplier[4] = {1, 1.2 , 1.8 , 2.5};
		float speed2;
		float top_speed_multiplier[4] =    {1, 1.05, 1.15, 1.15};
		float speed_cap = 1.2 * top_speed_multiplier[fuel];
		
		do{
			*position += *speed;
			
			// speed physics, minus "abs()" because train won't go backward.
			speed2 = *speed;
			*speed = fmaxf(0, *speed - (friction / weight));
			*speed += (10 * usfl * acceleration_multiplier[fuel]) / weight;
			*speed *= 1 - (air_resistance * 1000 / weight);
			
			*tick = *tick + 1;
		}	
		while(*speed <= speed_cap && (*speed - speed2) >= epsilon);
		
		return 0;
	}
Source : [Factorio locomotive wiki](https://wiki.factorio.com/Locomotive#Maximum_speed)

I feed variable pointers to the function to get multiple outputs : time in tick and distance in tile.

If for some reason your trains have wagons at the front, you have to change the program's air_resistance value to 0.01.

### Details on current acceleration function
The current code is slightly different : 
I'm using two geometric-arithmetic progression formulas to quickly get time and distance in one go, 
instead of waiting for the loop to complete.

Every tick, a new train speed is computed from an old train speed.

The acceleration equation has 3 terms : a friction term , an acceleration term and an aerodynamics term :

* fmaxf(0, speed - (friction / weight)), friction term. 
Let's shorten it to $f$.

* (10 * usfl * acceleration_multiplier[fuel]) / weight, acceleration term. 
Let's shorten it to $u$.

* 1 - (air_resistance * 1000 / weight), aerodynamic term. 
Let's shorten it to $a$.

In the factorio wiki formula, there is an additional abs() function, which is useful if the train is going backward. 
I ignore that part in this program because the train is only moving forward.

We will simplify that equation a bit, so that we can easily compute time and distance values rapidly.\
We basically have something that looks like this : next_speed = [max(0, speed - $f$) + $u$] * $a$.

1. If speed < friction, max(...) is equal to 0 and only $u.a$ is added to the speed. 
This is a way to make sure that even the biggest trains with huge friction will have an absolute minimum of speed, 
even if they were infinitelly long. In practice, the wagon limit is around 18-48 wagons per locomotive, depending on fuel.
2. Else, max(...) = speed - $f$, and next_speed = (speed - $f$ + $u$) * $a$.

I ignore that first case because in normal gameplay, with sane train length, 
the train will spend something like one tick in that state.

If we call the speed function $s_{t}$, where s increases depending on t (in tick), 
we can compute the next speed value $s_{t+1}$ from the old speed value $s_{t}$.

$s_{t+1} = (s_{t} - f + u) * a$

By rearanging a bit : $s_{t+1} = a*s_{t} + a(u - f)$. 
Something linear! ($y=ax+b$).

Here, we define two new variable : $b = a(u-f)$, and $r = \frac{b}{1 - a}$.

$s_{t} = a^t .(s_0 - r) + r$, is then the speed value for a given tick. $s_0$ is the initial speed (usually 0 tile/tick).

Therefore $s_{t} = a^t .(- r) + r = r(1 - a^t)$. 

Interestingly, for an infinite amount of ticks, the train will reach a speed of $r$ because $0 < a < 1$.

In practice, short nuclear trains quickly reach $r$, but coal trains with lots of wagons need thousands of ticks.
That's why I use $0.99 r$ as the speed the train will realisticaly reach in a sane time. 

Finally, we can compute the sum of all speeds, which is the distance the train will have traveled during all those ticks : 
$d_t = (u_0 - r)\frac{1 - a^t}{1 - a} + t.r$

Source : [Arithmetic-geometric progression formulas from French wikipedia](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tico-g%C3%A9om%C3%A9trique)

## Braking function
You can compute the braking time and braking distance with a loop like this :

	void braking_loop(int loco, int wagon, int research, float *speed, float *position, float *tick){

		float breaking_force = loco * 10 + wagon * 3;
		float weight = loco * 2000 + wagon * 1000;
		float friction = (loco + wagon) * 0.5;
		float multiplier[8] = {1, 1.1, 1.25, 1.4, 1.55, 1.7, 1.85, 2};

		while(*speed >= 0){
				
			*speed -= (breaking_force * multiplier[research]) / weight;
			*speed -= (friction / weight);
			*position += *speed;
			*tick = *tick + 1;
		}
		*tick += 10;
	}
Source : [Factorio forum thread](https://forums.factorio.com/viewtopic.php?t=108631)

Thanks to Tertius, mmmPI, farcast, FuryoftheStars and anyone helping on the thread.

### Details on current braking function
The current code is slightly different : 
I'm using two arithmetic progression formulas to quickly get time and distance in one go, 
instead of waiting for the loop to complete.

If we call the speed function $s_{t}$, where $s$ decreases depending on $t$ (in tick), 
we can compute the next speed value $s_{t+1}$ from the previous speed value $s_{t}$.

$s_{t+1} = s_{t} - k$, where k is the constant we substract every tick.

$s_{t} = s_0 - t.k$, is then the speed value for a given tick, with $s_0$ the initial speed (eg. 1.38 tile/tick).

We can compute the exact tick where speed equals zero :
 
$s_{t} = s_0 - t.k = 0$

$t = \frac{s_0}{k}$.

Finally, for $t = \frac{s_0}{k}$, we can compute the sum of all speeds, 
which is the distance the train will have traveled during all those ticks : 

$d_t = (t + 1)\frac{s_0 + s_{t}}{2}$

$d_t = (t + 1)\frac{s_0}{2}$

Source : [Arithmetic progression formulas from French wikipedia](https://fr.wikipedia.org/wiki/Suite_arithm%C3%A9tique)

## Throughput function
We compute general train values like weight, friction, etc.
Load and unload times are very easy to calculate because they're a function of inserters. I used values taken from the wiki.

Then we compute max speed of the train and call the acceleration and braking function once. 

If the train had enough distance between stations to reach terminal velocity, 
the coast time is easy to calculate and the throughput too.
Else, coast time will be set to 0, and the program will enter a loop that decreases max speed 
until the train's acceleration and braking distances are inferior to the distance between stations. 
I feel like there's a smarter way to do this[^1][^2], but it works.

[^1]: There is, but the resulting equation to quickly calculate $V_{max}$ (max_speed), 
and therefore the time it takes to accelerate and brake in a limited distance, looks like this : 
$$\frac{r.ln(1 - \frac{V_{max}}{r}) + V_{max}}{ln(a)} + \frac{1}{V_{max} + 10.k}(\frac{V_{max}^3}{2.k} + 10.V_{max}^2 + \frac{100.k}{2}.V_{max}) - d = 0$$
with $k$ the braking constant, $d$ the distance in tile (chunk * 32) and $a$ the aerodynamic term.
One anon helped me by mentioning both newton-raphson and bisection methods, to solve for $V_{max}$.
Problems: I don't know how to do that with an algorithm yet and I don't know if that will even make the program faster, 
as the only time when the program is in that state is for low (<50) chunk distances.

[^2]: Since then, I settled on a simpler formula thanks to /sci/ and /vg/ anons (thank you lads). 
The following equation has to be solved for $t_{max}$ : $$\frac{r^2}{2.k}(1 - a^{t_{max}})^2 + r(\frac{11}{2} - \frac{1}{1 - a})(1 - a^{t_{max}}) + r.t_{max}$$
Once I know $t_{max}$, I can easily compute the total time $t_{final}$ 
the train needs to accelerate and brake, because : $V_{max} = r(1 - a^{t_{max})$ and $t_{final} = t_{max} + \frac{V_{max}}{k} + 10 $.

## Main
Here we define the minimum and maximum width of the graph we want to plot.
The program needs a few values to compute acceleration and braking as well. 
They are stored in arrays, in case you want to compare the throughput of several trains.
We must first output the train configurations on the first line of the output file. 
We need that title line for gnuplot to name the data columns correctly.
Finally, we output data values for each chunk and throughput in blue belt.

## Gnuplot
```
set datafile separator ","

#set terminal pngcairo size 1080,1080
#set output "graph.png"

set key opaque
set title  "Compare throughput of different train configurations"
set xlabel "Distance (chunks)"
set ylabel "Throughput (blue belts)"
set grid

plot for [i=2:*] 'output.csv' u 1:i ti columnhead w lines lw 2

pause mouse button3
```
- You have to tell gnuplot that the input values are separated by colons.
- You can either make a png file (disabled here), or open the gnuplot program directly.
- I draw a white screen behind the column titles to make it easier to read. I define simple titles and a grid.
- I plot throughput (second column) over chunks (first column). 
If the user want to compare several trains, there will be several throughput columns, 
that's why i starts at 2 and ends at `*`.
- The Gnuplot window closes on right click.

The first iteration of this program used the gnuplot_i library but it was too simplistic for my ambitions : 
I wanted to use most of gnuplot's capabilities, such as linetypes, dashes, complex titles with input values, etc. 
I wanted something completelly automatic, but settled on computing the values first, and plotting the graph second.
That way, I can test a few different gnuplot functions on the fly without having to generate data all the time.  

## Work in progress
* [ ] If possible, improve the algorithm in the case where the train has to stop accelerating and start braking.
* [ ] Ask anons about the general program quality, clarity and memory management, and anything else that can be improved upon. Beyond gnuprof, start adding more warnings and using valgrind.
* [x] Fix terrible runtime with a profiler.
* [x] Fix precision issues from the braking algorithm.

In-game throughput validation allow me to safely go forward with the last step of the project : 
Making an easy to use, and preferably online, train throughput calculator for the community. 
From there, I don't really know where to go forward because my GUI knowledge is abysmal. 

What's easiest for the user and feasible by me? 
* Shareable HTML file? 
* Factorio Mod? 
* Some kind of program I can host on an external site like itch.io?

If so, should I use:
* Raylib? 
* gtk? 
* tcl/tk? or something else?

## Conclusion
I'm very happy with the results. 
Various in-game measurements show good model precision made possible by the hard work of motivated forum users.
While the acceleration formula was given on the wiki, it was a lot of work to find a precise braking formula 
(and this isn't the correct one from the game).
Both acceleration and braking models are precise enough for now.
