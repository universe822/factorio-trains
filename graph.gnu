#!/usr/bin/gnuplot

# input .csv separator beween values
set datafile separator ","

# output
#set terminal pngcairo size 1080,1080
#set output "graph.png"
                     
# key (legend), opaque=display a white screen behind the legend to make it easier to read
set key opaque

# Text
set title  "Compare throughput of different train configurations"
set xlabel "Distance (chunks)"
set ylabel "Throughput (blue belts)"

# tics
set grid

plot for [i=2:*] 'output.csv' u 1:i ti columnhead w lines lw 2

# Close gnuplot window on right click
pause mouse button3

